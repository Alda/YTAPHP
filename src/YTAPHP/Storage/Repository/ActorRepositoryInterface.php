<?php

namespace YTAPHP\Storage\Repository;

/**
 * Interface ActorRepositoryInterface
 * @author Alda <alda@leetchee.fr>
 */
interface ActorRepositoryInterface
{
    public function find($id);
}
