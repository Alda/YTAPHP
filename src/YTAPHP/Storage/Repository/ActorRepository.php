<?php

namespace YTAPHP\Storage\Repository;

use JamesMoss\Flywheel\Repository;
use YTAPHP\Exception\ActorNotFoundException;
use YTAPHP\Exception\DuplicateActorException;
use YTAPHP\Entity\ActorFactory;
use YTAPHP\Entity\Actor;
use JamesMoss\Flywheel\Document;

/**
 * Class ActorRepository
 * @author Alda <alda@leetchee.fr>
 */
class ActorRepository implements ActorRepositoryInterface
{
    /**
     * @var Repository
     */
    private $repo;

    public function __construct(Repository $repo)
    {
        $this->repo = $repo;
    }

    public function find($id)
    {
        $flatActor = $this->repo
            ->query()
            ->where('id', '==', $id)
            ->execute()
            ->first();

        if (!$flatActor) {
            throw new ActorNotFoundException(sprintf(
                'Actor #%s cannot be found',
                $id
            ));
        }

        return ActorFactory::fromDocument($flatActor);
    }

    public function insert(Actor $actor)
    {
        try {
            $this->find($actor->getPreferredUsername());
            throw new DuplicateActorException(sprintf(
                'Actor "%s" already exists',
                $actor->getPreferredUsername()
            ));
        } catch (ActorNotFoundException $e) {
            $document = ActorFactory::toDocument($actor);
            $document->setId($actor->getPreferredUsername());
            $this->repo->store($document);
        }
    }
}
