<?php

namespace YTAPHP\Storage\Repository;

use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use JamesMoss\Flywheel\Config;
use JamesMoss\Flywheel\Repository;

/**
 * Class ActorRepositoryFactory
 * @author Alda <alda@leetchee.fr>
 */
class ActorRepositoryFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ) : ActorRepository {
        $repo = new Repository(
            'actor',
            $container->get(Config::class)
        );

        return new ActorRepository($repo);
    }
}
