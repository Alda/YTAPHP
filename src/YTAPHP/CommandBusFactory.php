<?php

namespace YTAPHP;

use Interop\Container\ContainerInterface;
use League\Tactician\Handler\MethodNameInflector\InvokeInflector;
use League\Tactician\Container\ContainerLocator;
use League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor;
use League\Tactician\Handler\CommandHandlerMiddleware;
use League\Tactician\CommandBus;
use YTAPHP\Command\DisplayActor;
use YTAPHP\Command\DisplayActorHandler;
use YTAPHP\Command\CreateNewActorCommandHandler;
use YTAPHP\Command\CreateNewActorCommand;

class CommandBusFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $inflector = new InvokeInflector;

        $commandsMapping = [
            DisplayActor::class => DisplayActorHandler::class,
            CreateNewActorCommand::class => CreateNewActorCommandHandler::class
        ];

        $locator = new ContainerLocator($container, $commandsMapping);

        $nameExtractor = new ClassNameExtractor;
        $commandHandlerMiddleware = new CommandHandlerMiddleware(
            $nameExtractor,
            $locator,
            $inflector
        );

        return new CommandBus([$commandHandlerMiddleware]);
    }
}
