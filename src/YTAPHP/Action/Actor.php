<?php

namespace YTAPHP\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use YTAPHP\Command\DisplayActor;
use Zend\Diactoros\Response\JsonResponse;
use League\Tactician\CommandBus;
use Zend\Expressive\Router\RouterInterface;

/**
 * @author Alda <alda@leetchee.fr>
 */
class Actor implements MiddlewareInterface
{
    public function __construct(CommandBus $commandBus, RouterInterface $router)
    {
        $this->commandBus = $commandBus;
        $this->router = $router;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $command = new DisplayActor($request->getAttributes()['actor']);

        try {
            $actor = $this->commandBus->handle($command);
        } catch (\YTAPHP\Exception\ActorNotFoundException $e) {
            return new JsonResponse(['error' => $e->getMessage()], 404);
        }

        return new JsonResponse($actor);
    }
}
