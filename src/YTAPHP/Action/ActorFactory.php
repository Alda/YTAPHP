<?php

namespace YTAPHP\Action;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use YTAPHP\Action\Actor;
use League\Tactician\CommandBus;

class ActorFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return Actor
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new Actor(
            $container->get(CommandBus::class),
            $container->get(\Zend\Expressive\Router\RouterInterface::class)
        );
    }
}
