<?php

namespace YTAPHP\Entity;

use YTAPHP\Entity\Actor;
use JamesMoss\Flywheel\Document;

/**
 * Class ActorFactory
 * @author Alda <alda@leetchee.fr>
 */
class ActorFactory
{
    private function __construct()
    {
    }

    public static function fromDocument(Document $actor)
    {
        return new Actor(
            $actor->preferredUsername,
            $actor->name,
            $actor->icon,
            $actor->image,
            $actor->summary,
            $actor->url,
            $actor->privateKey,
            $actor->publicKey
        );
    }

    public static function toDocument(Actor $actor)
    {
        return new Document([
            'id' => $actor->getPreferredUsername(),
            'preferredUsername' => $actor->getPreferredUsername(),
            'name' => $actor->getName(),
            'icon' => $actor->getIcon(),
            'image' => $actor->getImage(),
            'summary' => $actor->getSummary(),
            'url' => $actor->getUrl(),
            'privateKey' => $actor->getPrivateKey(),
            'publicKey' => $actor->getPublicKey()
        ]);
    }
}
