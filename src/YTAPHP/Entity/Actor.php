<?php

namespace YTAPHP\Entity;

/**
 * Class Actor
 * @author Alda <alda@leetchee.fr>
 */
class Actor
{
    const TYPE = 'Person';

    private $preferredUsername;
    private $name;
    private $icon;
    private $image;
    private $summary;
    private $url;
    private $privateKey;
    private $publicKey;

    public function __construct(
        string $preferredUsername,
        string $name,
        string $icon,
        string $image,
        string $summary,
        string $url,
        string $privateKey,
        string $publicKey
    ) {
        $this->preferredUsername = $preferredUsername;
        $this->name = $name;
        $this->icon = $icon;
        $this->image = $image;
        $this->summary = $summary;
        $this->url = $url;
        $this->privateKey = $privateKey;
        $this->publicKey = $publicKey;
    }

    public function getId()
    {
        return $this->preferredUsername;
    }

    public function getPreferredUsername()
    {
        return $this->preferredUsername;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getIcon()
    {
        return $this->icon;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getSummary()
    {
        return $this->summary;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getPrivateKey()
    {
        return $this->privateKey;
    }

    public function getPublicKey()
    {
        return $this->publicKey;
    }

    public function toArray()
    {
        return [
            'preferredUsername' => $this->preferredUsername,
            'name' => $this->name,
            'icon' => [
                'type' => 'Image',
                'url' => $this->icon
            ],
            'image' => [
                'type' => 'Image',
                'url' => $this->image
            ],
            'summary' => $this->summary,
            'url' => $this->url,
            'publicKey' => [
                'publicKeyPem' => $this->publicKey
            ]
        ];
    }
}
