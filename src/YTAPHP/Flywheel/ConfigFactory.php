<?php

namespace YTAPHP\Flywheel;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use JamesMoss\Flywheel\Config;

/**
 * Class ConfigFactory
 * @author Alda <alda@leetchee.fr>
 */
class ConfigFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ) : Config {
        $config = $container->get('config');
        return new Config($config['flywheel']['path']);
    }
}
