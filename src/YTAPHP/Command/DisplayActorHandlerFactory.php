<?php

namespace YTAPHP\Command;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use YTAPHP\Command\DisplayActorHandler;
use YTAPHP\Storage\Repository\ActorRepository;

class DisplayActorHandlerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new DisplayActorHandler($container->get(ActorRepository::class));
    }
}
