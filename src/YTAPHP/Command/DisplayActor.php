<?php

namespace YTAPHP\Command;

/**
 * Class DisplayActor
 * @author Alda <alda@leetchee.fr>
 */
class DisplayActor
{
    private $actor;

    public function __construct(string $actor)
    {
        $this->actor = $actor;
    }

    public function getActor() : string
    {
        return $this->actor;
    }
}
