<?php

namespace YTAPHP\Command;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use YTAPHP\Storage\Repository\ActorRepository;
use YTAPHP\CryptoManager;

/**
 * Class CreateNewActorCommandHandlerFactory
 * @author Alda <alda@leetchee.fr>
 */
class CreateNewActorCommandHandlerFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ) : CreateNewActorCommandHandler {
        return new CreateNewActorCommandHandler(
            $container->get(ActorRepository::class),
            $container->get(CryptoManager::class)
        );
    }
}
