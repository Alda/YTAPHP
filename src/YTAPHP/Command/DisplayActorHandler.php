<?php

namespace YTAPHP\Command;

use YTAPHP\Storage\Repository\ActorRepositoryInterface;

class DisplayActorHandler
{
    /**
     * @var ActorRepositoryInterface
     */
    private $actorRepository;

    public function __construct(
        ActorRepositoryInterface $actorRepository
    ) {
        $this->actorRepository = $actorRepository;
    }

    public function __invoke(DisplayActor $command)
    {
        $actor = $this->actorRepository->find($command->getActor());
        return $actor->toArray();
    }
}
