<?php

namespace YTAPHP\Command;

/**
 * Class CreateNewActorCommand
 * @author Alda <alda@leetchee.fr>
 */
class CreateNewActorCommand
{
    private $username;
    private $icon;
    private $image;
    private $url;

    public function __construct(
        string $username,
        string $icon,
        string $image,
        string $url
    ) {
        $this->username = $username;
        $this->icon = $icon;
        $this->image = $image;
        $this->url = $url;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getIcon()
    {
        return $this->icon;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getUrl()
    {
        return $this->url;
    }
}
