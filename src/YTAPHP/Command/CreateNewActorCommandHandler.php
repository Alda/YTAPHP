<?php

namespace YTAPHP\Command;

use YTAPHP\CryptoManager;
use YTAPHP\Storage\Repository\ActorRepositoryInterface;
use YTAPHP\Entity\Actor;

/**
 * Class CreateNewActorCommandHandler
 * @author Alda <alda@leetchee.fr>
 */
class CreateNewActorCommandHandler
{
    /**
     * @var ActorRepositoryInterface
     */
    private $repo;

    /**
     * @var CryptoManager
     */
    private $crypto;

    public function __construct(
        ActorRepositoryInterface $repo,
        CryptoManager $crypto
    ) {
        $this->repo = $repo;
        $this->crypto = $crypto;
    }

    public function __invoke(CreateNewActorCommand $command)
    {
        $cryptoKeys = $this->crypto->generate();

        $actor = new Actor(
            $command->getUsername(),
            $command->getUsername(),
            $command->getIcon(),
            $command->getImage(),
            '',
            $command->getUrl(),
            $cryptoKeys['private'],
            $cryptoKeys['public']
        );

        $this->repo->insert($actor);
    }
}
