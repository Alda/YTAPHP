<?php

namespace YTAPHP\Exception;

/**
 * Class ActorNotFoundException
 * @author Alda <alda@leetchee.fr>
 */
class ActorNotFoundException extends \RuntimeException
{
}
