<?php

namespace YTAPHP\Exception;

/**
 * Class DuplicateActorException
 * @author Alda <alda@leetchee.fr>
 */
class DuplicateActorException extends \RuntimeException
{
}
