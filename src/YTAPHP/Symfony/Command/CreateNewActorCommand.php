<?php

namespace YTAPHP\Symfony\Command;

use Symfony\Component\Console\Command\Command;
use League\Tactician\CommandBus;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use YTAPHP\Command\CreateNewActorCommand as CreateNewActorCmd;

/**
 * Class CreateNewActor
 * @author Alda <alda@leetchee.fr>
 */
class CreateNewActorCommand extends Command
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('actor:create')
            ->setDescription('Creates a new actor from the ground up')
            ->addArgument('username', InputArgument::REQUIRED, "Le nom d'utilisateur")
            ->addArgument('icon', InputArgument::REQUIRED, "L'avatar du compte")
            ->addArgument('image', InputArgument::REQUIRED, "L'image du profil")
            ->addArgument('url', InputArgument::REQUIRED, "L'url distante");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $command = new CreateNewActorCmd(
            $input->getArgument('username'),
            $input->getArgument('icon'),
            $input->getArgument('image'),
            $input->getArgument('url')
        );

        $this->commandBus->handle($command);
    }
}
