<?php

namespace YTAPHP;

/**
 * Class CryptoManager
 * @author Alda <alda@leetchee.fr>
 */
class CryptoManager
{
    const PARAMS = [
        'digest_alg' => 'sha256',
        'private_key_bits' => 4096,
        'private_key_type' => OPENSSL_KEYTYPE_RSA
    ];

    public function generate()
    {
        $resource = openssl_pkey_new(self::PARAMS);
        openssl_pkey_export($resource, $private);
        $public = openssl_pkey_get_details($resource)['key'];

        return ['public' => $public, 'private' => $private];
    }
}
