<?php

namespace YTAPHP;

use League\Tactician\CommandBus;

/**
 * The configuration provider for the App module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array
     */
    public function __invoke()
    {
        return [
            'flywheel' => $this->getFlywheel(),
            'dependencies' => $this->getDependencies(),
            'templates'    => $this->getTemplates(),
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            'invokables' => [
                CryptoManager::class
            ],
            'factories'  => [
                CommandBus::class => CommandBusFactory::class,
                Action\Actor::class => Action\ActorFactory::class,
                Command\DisplayActorHandler::class => Command\DisplayActorHandlerFactory::class,
                Command\CreateNewActorCommandHandler::class => Command\CreateNewActorCommandHandlerFactory::class,
                Storage\Repository\ActorRepository::class => Storage\Repository\ActorRepositoryFactory::class,
                \JamesMoss\Flywheel\Config::class => Flywheel\ConfigFactory::class
            ],
        ];
    }

    /**
     * Returns the templates configuration
     *
     * @return array
     */
    public function getTemplates()
    {
        return [
            'paths' => [
                'app'    => ['templates/app'],
                'error'  => ['templates/error'],
                'layout' => ['templates/layout'],
            ],
        ];
    }

    public function getFlywheel()
    {
        return ['path' => getcwd() . '/data/flywheel'];
    }
}
