<?php

use League\Tactician\CommandBus;
use Symfony\Component\Console\Application;
use YTAPHP\Symfony\Command\CreateNewActorCommand;

chdir(dirname(__DIR__));
require 'vendor/autoload.php';

/**
 * Self-called anonymous function that creates its own scope and keep the global namespace clean.
 */
call_user_func(function () {
    /** @var \Interop\Container\ContainerInterface $container */
    $container = require 'config/container.php';
    $commandBus = $container->get(CommandBus::class);

    $application = new Application;
    $application->add(new CreateNewActorCommand($commandBus));

    $application->run();
});
