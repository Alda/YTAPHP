<?php
/**
 * Setup routes with a single request method:
 *
 * $app->get('/', App\Action\HomePageAction::class, 'home');
 * $app->post('/album', App\Action\AlbumCreateAction::class, 'album.create');
 * $app->put('/album/:id', App\Action\AlbumUpdateAction::class, 'album.put');
 * $app->patch('/album/:id', App\Action\AlbumUpdateAction::class, 'album.patch');
 * $app->delete('/album/:id', App\Action\AlbumDeleteAction::class, 'album.delete');
 *
 * Or with multiple request methods:
 *
 * $app->route('/contact', App\Action\ContactAction::class, ['GET', 'POST', ...], 'contact');
 *
 * Or handling all request methods:
 *
 * $app->route('/contact', App\Action\ContactAction::class)->setName('contact');
 *
 * or:
 *
 * $app->route(
 *     '/contact',
 *     App\Action\ContactAction::class,
 *     Zend\Expressive\Router\Route::HTTP_METHOD_ANY,
 *     'contact'
 * );
 */

$app->get('/{actor}', YTAPHP\Action\Actor::class, 'actor');
//$app->get('/{actor}/activity/{id}', YTAPHP\Action\Activity::class, 'activity');
//$app->get('/{actor}/outbox', YTAPHP\Action\Outbox::class, 'outbox');
//$app->get('/{actor}/following', YTAPHP\Action\Following::class, 'following');
//$app->get('/{actor}/followers', YTAPHP\Action\Followers::class, 'followers');
//$app->post('/{actor}/inbox', YTAPHP\Action\Inbox::class, 'inbox');
